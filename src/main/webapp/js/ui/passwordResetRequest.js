/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var page = (function() {
    function init() {
        $("#requestPasswordResetForm").on("submit", submitForm);
    }

    function submitForm() {
        $("#okButton").attr("disabled", "disabled");
        var username = $("#requestPasswordResetForm input[name='username']").val();
        unoreader.service.passwordRecovery.requestPasswordReset(username, requestResetSuccess, requestResetError);
        return false;
    }

    function requestResetSuccess() {
        renderMessage({
            cssClass: "alert-success",
            title: "An email has been sent.",
            text: "Please check your inbox and follow the instructions to reset your password."
        });
    }

    function requestResetError() {
        $("#okButton").removeAttr("disabled");
        renderMessage({
            cssClass: "alert-error",
            title: "Your email is not a valid account.",
            text: "Pleasy check your email address."
        });
    }

    function renderMessage(message) {
        $("#infoArea").html($("#messageTemplate").render(message));
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    page.init();
});