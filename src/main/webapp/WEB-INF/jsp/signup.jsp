<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container-fluid ur-hero">
    <div class="row">
        <div class="container">
            <h1>Sign up for your <span class="ur-highlight">free</span> account</h1>
            <h2>Create your very own news channels, manage your newfeeds, discover new sites and much more</h2>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Create a new account</h3></div>
                <form id="signupForm">
                    <div class="ur-content ur-extra-padding">
                        <div id="infoArea"></div>
                        <label>Email</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="email" class="input-xlarge" name="username" placeholder="Your email address..." required="required" maxlength="50"/>
                        </div>
                        <label>Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="password" placeholder="Type a password..." required="required" pattern=".{5,25}" />
                        </div>
                        <label>Retype Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="password2" placeholder="Retype the password..." required="required" pattern=".{5,25}"/>
                        </div>
                    </div>
                    <div class="ur-actions">
                        <input class="btn btn-success" type="submit" value="Create Account"/>
                        <a class="btn" href="<c:url value="/"/>">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--********************************************************************
                            TEMPLATES
*********************************************************************-->
<script id="errorMessageTemplate" type="text/x-jsrender">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>{{:title}}</h4>
        {{:text}}
    </div>
</script>