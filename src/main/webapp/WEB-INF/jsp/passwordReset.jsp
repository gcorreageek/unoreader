<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="span6 offset3">
            <div class="ur-lightbox ur-extra-margin">
                <div class="ur-header"><h3>Enter your new password</h3></div>
                <form id="resetForm">
                    <div class="ur-content ur-extra-padding">
                        <input type="hidden" name="token" value="${param.token}"/>
                        <div id="infoArea"></div>
                        <label>New Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="newPassword1" required="required" placeholder="Type a new password..." pattern=".{5,25}" />
                        </div>
                        <label>Retype Password</label>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-key"></i></span>
                            <input type="password" class="input-xlarge" name="newPassword2" required="required" placeholder="Retype the new password..." pattern=".{5,25}"/>
                        </div>
                    </div>
                    <div class="ur-actions">
                        <input class="btn btn-success" type="submit" value="Save Password"/>
                        <a class="btn" href="<c:url value="/"/>">Cancel</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<!--********************************************************************
                            TEMPLATES
*********************************************************************-->
<script id="errorMessageTemplate" type="text/x-jsrender">
    <div class="alert {{:cssClass}}">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>{{:title}}</h4>
        {{:text}}
    </div>
</script>