/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.Feed;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.FeedRepository;
import com.ideasagiles.reader.service.FeedService;
import com.ideasagiles.reader.service.UserService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@Transactional
public class FeedServiceImpl implements FeedService {

    @Autowired
    private UserService userService;
    @Autowired
    private FeedRepository feedRepository;


    /**
     * Saves a new feed for a user. This method's transaction propagation
     * is set to REQUIRES_NEW to allow bulk inserts (from packs).
     * @param feed The Feed to save.
     * @throws DataIntegrityException if a feed with the same url/channel/user already exists.
     */
    @PreAuthorize("isAuthenticated()")
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void save(Feed feed) {
        feed.setUser(userService.findLoggedInUser());
        feed.setCreatedOn(new Date());

        feedRepository.save(feed);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public List<Feed> findAll() {
        return feedRepository.findByUser(userService.findLoggedInUser());
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void delete(long feedId) {
        Feed feed = feedRepository.findOne(feedId);
        checkPermissions(feed);
        feedRepository.delete(feed);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void update(Feed feedData) {
        Feed feed = feedRepository.findOne(feedData.getId());
        checkPermissions(feed);
        if (StringUtils.hasText(feedData.getUrl())) {
            feed.setUrl(feedData.getUrl());
        }
        if (StringUtils.hasText(feedData.getChannel())) {
            feed.setChannel(feedData.getChannel());
        }
        if (StringUtils.hasText(feedData.getName())) {
            feed.setName(feedData.getName());
        }
    }

    /** Checks if the logged in user has permissions on the given Feed.
     * @param feed the Feed to check.
     * @throws AccessDeniedException if the user does no has permissions.
     */
    private void checkPermissions(Feed feed) {
        User user = userService.findLoggedInUser();
        if (!feed.getUser().getId().equals(user.getId())) {
            throw new AccessDeniedException("Feed does not belong to user.");
        }
    }
}
