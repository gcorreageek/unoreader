/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.vo.UserUpdateVo;
import java.util.List;

public interface UserService {

    /**
     * Saves a new User, hashing its password and asigning default values.
     * After saving, the User will have an unique identifier, and a hashed
     * password.
     * @param user the User to save.
     * @param plainTextPassword the password in plain text.
     */
    void save(User user, String plainTextPassword);

    /**
     * Updates the logged in user with the given information.
     * If the old password and new password is set (and the old passwords matches
     * the current password), then the password is also updated.
     * @param user the User to update.
     */
    void updateLoggedInUser(UserUpdateVo user);

    /**
     * Finds the user that is logged in. It throws a Security Exception is the
     * user is not logged in.
     */
    User findLoggedInUser();

    /**
     * Finds all users. It throws a Security Exception if the user is not admin.
     */
    List<User> findAll();
}
