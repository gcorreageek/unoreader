/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.vo;

public class ChannelUpdateVo {

    private String oldChannel;
    private String newChannel;

    public String getOldChannel() {
        return oldChannel;
    }

    public void setOldChannel(String oldChannel) {
        this.oldChannel = oldChannel;
    }

    public String getNewChannel() {
        return newChannel;
    }

    public void setNewChannel(String newChannel) {
        this.newChannel = newChannel;
    }
}
