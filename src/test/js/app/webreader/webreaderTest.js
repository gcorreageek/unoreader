/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

asyncTest("loadFeed, valid newsfeed url, returns feeds", 17, function() {
    var options = {
        url: "http://feeds.arstechnica.com/arstechnica/index",
        numEntries: 6,
        success: checkTest,
        error: failTest
    };

    unoreader.webreader.loadFeed(options);

    function checkTest(result) {
        var i;
        var entries;
        ok(result, "result should not be null or undefined");
        ok(result.feed, "result.feed should not be null or undefined");
        ok(result.feed.entries, "result should not be null or undefined");
        entries = result.feed.entries;

        ok(entries, "feeds must not be null or undefined");
        ok(entries.length === options.numEntries, "there should many entries");
        for (i = 0; i < entries.length; i++) {
            ok(entries[i].title, "the title must be setted");
            ok(entries[i].content, "the content must be setted");
        }
        start();
    }

    function failTest(result) {
        ok("The call should not fail", false);
        start();
    }
});

asyncTest("loadFeed, invalid newsfeed url, calls error", 3, function() {
    var options = {
        url: "http://www.slashdot.org",
        numEntries: 6,
        success: failTest,
        error: checkTest
    };

    unoreader.webreader.loadFeed(options);

    function checkTest(result) {
        ok(result, "result should not be null or undefined");
        ok(result.error, "result error should not be null or undefined");
        ok(result.error.message, "result error message should not be null or undefined");
        start();
    }

    function failTest(result) {
        ok("The call should not fail", false);
        start();
    }
});