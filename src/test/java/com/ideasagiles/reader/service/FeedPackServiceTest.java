/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.SecurityTestUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;
import static org.junit.Assert.*;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class FeedPackServiceTest extends AbstractDatabaseTest {

    @Autowired
    private FeedPackService feedPackService;

    @Test
    public void addPackToUser_validChannel_addsFeeds() {
        SecurityTestUtils.loginTestUser();
        int beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "feed");
        feedPackService.addPackToUser("comedy");
        int afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "feed");
        assertTrue("Should add new feeds", afterCount > beforeCount);

        beforeCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "feed");
        feedPackService.addPackToUser("comedy");
        feedPackService.addPackToUser("comedy");
        afterCount = JdbcTestUtils.countRowsInTable(jdbcTemplate, "feed");
        assertTrue("Should ignore duplicates", afterCount == beforeCount);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void addPackToUser_userIsNotLoggedIn_throwsSecurityException() {
        SecurityTestUtils.logout();
        feedPackService.addPackToUser("comedy");
    }

}
